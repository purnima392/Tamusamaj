<?php include('header.php');
       require('AdminLTE/inc/config.php');
        $BlogId=$_GET['id'];
        $latBlog=$mysqli->query("select * from blog where BlogId=$BlogId");
        $SiPackage=$latBlog->fetch_array();
        $Title=$SiPackage["Title"];
        $Description=$SiPackage["Description"];
        $Photo=$SiPackage["Photo"];
        $CreatedOn=$SiPackage["CreatedOn"];
        if(isset($_POST['btnSubmit'])){
        	$Name=$_POST["txtName"];
        	$Subject=$_POST["txtSubject"];
            $Message=$_POST["txtMessage"];
            $add_sql = $mysqli->query("INSERT INTO comments SET Comment='$Message',BlogId = '$BlogId',
            	Subject='$Subject',CommentedBy='$Name'");
             if($add_sql = TRUE){
                 $successMsg = '<div class="alert alert-success">Successfully blog Added</div>';
                 echo "<meta http-equiv='refresh' content='0'>";
                 echo "<script>alert('Thanks for your comment.');
                       window.location.href=window.location;
                       </script>";
             }else{
                 $successMsg = '<div class="alert alert-success">Some Error!!! Contact to Web Page Nepal for IT Help.</div>';
             }
        }	
?>
<html>
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <title></title>
  </head>
  <body>
    <section class="content inner-content">
      <div class="container">
        <div class="row mb-20">
          <div class="col">
            <h2 class="innertitle">
              <?=$Title?>
              </h2>
          </div>
          <div class="col text-right">
            <a href="index.php" class="breadcrumb">Back to home</a>
          </div>
        </div>
        <article class="blogpost blog-detail">
          <div class="blog-img">
            <img src="img/<?=$Photo?>" />
          </div>
          <div class="blogpost-body">
            <div class="blog-title">
              <h2 class="title">
                <a href="#">
                  <?=$Title?>
                  </a>
              </h2>
              <div class="post-info">
                <span>
                  <?=$CreatedOn?>
                  </span>
              </div>
              <div class="comment">
                <a href="#">22 comments</a>
              </div>
            </div>
            <div class="blogpost-content">
              <?=$Description?>
              </div>
          </div>
        </article>
        <hr />
        <div class="comments">
        <?php 
        $latBlog=$mysqli->query("select * from comments where BlogId=$BlogId order by CommentId DESC");
        $Count=$latBlog->num_rows;
        ?>
        <h2 class="title">There are <?=$Count?> comments</h2>
        <?php 
        while($SiPackage=$latBlog->fetch_array()){
	        $Subject=$SiPackage["Subject"];
	        $CommentId=$SiPackage["CommentId"];
	        $CommentedBy=$SiPackage["CommentedBy"];
	        $Comment=$SiPackage["Comment"];
	        $CreatedOn=$SiPackage["Created_Date"];
        ?>

          

          <!-- comment start -->
          <div class="comment clearfix">
            <div class="comment-avatar">
              <img src="img/avatar.jpg" alt="avatar" />
            </div>
            <div class="comment-content">
              <h3><?=$Subject?></h3>
              <div class="comment-meta">By 
              <a href="#"><?=$CommentedBy?></a> | <?=$CreatedOn?></div>
              <div class="comment-body clearfix">
               <?=$Comment?>
              <!--   <a href="blog-post.html" class="btn btn-gray more pull-right"> Reply</a> -->
              </div>
            </div>
            <!-- comment start
            <div class="comment clearfix">
              <div class="comment-avatar">
                <img src="img/avatar.jpg" alt="avatar" />
              </div>
              <div class="comment-content clearfix">
                <h3>Comment title</h3>
                <div class="comment-meta">By 
                <a href="#">admin</a> | Today, 12:31</div>
                <div class="comment-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                  magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                  commodo</p>
                  <a href="blog-post.html" class="btn btn-gray more pull-right"> Reply</a>
                </div>
              </div>
            </div> 
            comment end -->
          </div>
          <!-- comment end -->
          <!-- comment start -->
         <?php } ?>
          <!-- comment end -->
        </div>
        <hr />
        <div class="contact-form">
          <h2 class="innertitle">Leave a comment</h2>
          <form method="post" >
            <div class="form-group">
            <label for="lblName">Name</label> 
            <input type="text" class="form-control" id="txtName" name="txtName" aria-describedby="name" /></div>
            <div class="form-group">
            <label for="lblSubject">Subject</label> 
            <input type="text" class="form-control" id="txtSubject" name="txtSubject" aria-describedby="subject" /></div>
            <div class="form-group">
            <label for="exampleFormControlTextarea1">Message</label> 
            <textarea class="form-control" id="txtMessage" name="txtMessage" rows="3"></textarea></div>
            <button type="submit" class="btn btn-primary" name="btnSubmit">Submit</button>
          </form>
        </div>
      </div>
    </section>
    <?php include('footer.php')?>
  </body>
</html>
