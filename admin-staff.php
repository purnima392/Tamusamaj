<?php include('header.php');
      require('AdminLTE/inc/config.php');
      $CategoryId=$_GET["id"];
      if($CategoryId==1) {
        $Title="Admin Staff"; }
      else if($CategoryId==2)
        $Title="Teaching Staff";
      else 
        $Title="Non-Teaching Staff";
?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">

        <h2 class="innertitle"><?=$Title?></h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> 
      <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <div class="teamList">
    	<div class="row">
        <?php
            $latProjects=$mysqli->query("SELECT * from staff where CategoryId=$CategoryId");
            while($SiPackage=$latProjects->fetch_array()){
               $StaffId=$SiPackage["StaffId"];
               $Name=$SiPackage["Name"];
               $Designation=$SiPackage["Designation"];
               if($SiPackage["SubCategory"]!=""){
               $SubCategory='('.$SiPackage["SubCategory"].')';
               }
               else $SubCategory='';
               $Photo=$SiPackage["Photo"];
        ?>
        	<div class="col-lg-3 col-md-3">
            	<div class="team">
                	<div class="teamImg"><img data-src="holder.js/255x255?bg=fbba05"></div>
                    <div class="teamInfo"><h3><?=$Name?></h3>

                    	<h4><?=$Designation?><?=$SubCategory?></h4>                        
                    </div>                    
                </div>
            </div>
         <?php } ?>
           
        </div>
    </div>
  </div>
</section>
<?php include('footer.php')?>