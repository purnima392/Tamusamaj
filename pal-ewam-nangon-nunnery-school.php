<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			
					
					
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Pal Ewam Namgon Nunnery School</h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						<div class="square-img"><img src="img/56.jpg" alt=""></div>
						<p>In June 2005, Mrs Francesca Stengel and Maraigrazia, two Italian women met Ven. Khenpo Tsewang Rigzin (the Abbot of Namgyal Monastery and Founder of Monastic School) and the former school Principal Ven. Lekshay Tendhar. The two women suggested the establishment of Nunnery school in order to provide modern education to young girls from poor family. But the financial constraints made it impossible for the Khenpo la to build a new school for nuns. </p>
<p>By their next visit in Upper Mustang in 2008, a family from Namgyal Village had offered a plot of land to the Khenpo la for the establishment of Nunnery School.  On their following trip in Upper Mustang on 2010, Mrs. Francesca Stengel and Maraigrazia offered to sponsor fooding and teacher salary for the Nunnery School. On 2012 the construction of Nunnery school was started. During that time 18 nuns with 3 teachers and 1 cook were there in the school. In this way Nunnery school was started by Mrs. Francesca Stengel and her team with Khenpo Tsewang la. </p>

<p>Geographically, Upper Mustang is in very high altitude and it is very cold in winter season. The students are not able to run their daily classes smoothly due to cold climatic condition there. And then the nuns and staffs were taken to Pokhara for winter academic session where house was rented near the lakeside. The situation was very tough at that time; rooms were limited to nuns for their classes and rooms. Later Khenpo la bought a plot of land in the hill of Khapaudi, near Lakeside. With the support and donors from Singapore Dharma friends finally Nunnery school construction was started in the year 2014. Since then our started shift to their own new building and started their winter class in Khapaudi. </p>
<p>
		Recently, August 12th 2017, new classroom building was built by Love @ Pal organization, Singapore. Five new rooms were made as a classroom for the nuns. Currently we have 50 nuns ranging Kindergarten to grade 7 with 6 teaching staffs.
</p>



								
				
			
		</div>
</section>


 



<?php include('footer.php')?>