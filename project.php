<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			
					
					
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Ongoing Project</h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						
						<p>In order to obtain the necessary funds of monks and maintenance of the school and monastery, Venerable Khenpo Tsewang Rigzin and monks visit various Buddhist center in Singapore and Malaysia. These visits are initiated by the request and invitation from Dharma friends and well-wishers from there. During their visit they perform various Buddhist traditional ritual arts and activities, create and mandala, public teaching etc. Buddhist yoga practices and performs puja as requested by the individuals. The main purpose of the religious tours is to raise funds for the children’s basic needs and the school building projects. The main aim and target for this religious tour is to make donation and look after the needs of the school. Not only had that it also spread the message of Buddha to the rest of the world.</p>
						<div class="square-img-right mt-20"><img src="img/bg.jpg" alt=""></div>
						<h2 class="innertitle mt-20">MAHAKALA PUJA</h2>
<p>Mahakala is the wrathful manifestation, embodying the fierce compassionate activity of all Buddhas. Wrathful compassion of Mahakala is able to overcome any obstacles and negativities one faces on the path to enlightenment.</p>

<p>Mahakala, is also known as the Chief Dharmapala, is the guardian protector of Buddha’s teachings. Dharmapalas are basically emanations of Buddha’s who undertake the role of removing obstacles on the path to liberation. He can be practiced as the Three Roots-Lama, Yidam and Protector.</p>

<p>There are many different colors and forms of Mahakala, but generally he is recognized universally in all schools of Vajrayana as one of the prominent guardians and great protectors of the Dharma.</p>

<p>For example, Maning Mahakala of the Nyingma Tradition, Mahakala Bernakchen of the Karma Kagyu School, the Four-Headed Mahakala of Sakya Tradition and Six-Armed form of the Mahakala is the principal protector of the Gelugpa.</p> 
<blockquote>
		The objective of Mahakala practice is to assist practitioners in removing any challenges that impedes their spiritual practice, as well as to encourage exertion and devotion and at the same time purify obscuration and defilements. With earnest prayer and diligent practices, blessings will be bestowed and obstacles will be pacified.
</blockquote>
<div class="square-img"><img src="img/050909tarapuja-2.jpg" alt=""></div>
<h2 class="innertitle mt-20">TARA PUJA</h2>
<p>Tara is the female manifestation of the Buddha’s omniscient mind.</p>
<blockquote><em>"Reciting the 21 Taras prayer with devotion-remembering Tara singing praises and reciting mantras at any time of the day or night-protects you from fear and dangers, and fulfills all your wishes."</em></blockquote>								
				
			<p>The colour green is associated with the air element. Hence, Green Tara is especially swift in granting the wishes of sentient beings. Tara is also known as the “Mother of all Buddhas” because she embodies wisdom-all Buddhas are born from this wisdom.</p>
			<p>Tara is quick to respond our prayers. Here are some of the benefits of praying to Tara. In the future , you will:</p>
			<ul class="list">
					<li>Achieve success in your career and business</li>
					<li>Increase your wealth</li>
			</ul>
			<p>
					<b>Tara Mantra:</b> Anyone can recite the mantra of Green Tara. The prayer of Tara has many extraordinary blessings. It is helpful to recite these prayers whenever one is troubled and wishes to seek solace.
			</p>
			<p><b>OM TARE TUTTARE TURE SVAHA</b></p>
			<p><b>Means:</b> "I prostrate to Tara the Liberator, Mother of all the Victorious Ones."</p>
			<hr/>
			<div class="square-img-right"><img src="img/Vajrapani-Fire-Puja-300x200.jpg" alt=""></div>
			<h2 class="innertitle mt-20">VAJRAPANI FIRE PUJA</h2>
			<p>The Vajrapani Fire Puja, presided over by his Holiness Jigdal Dagchen Sakya Rinpoche, was held on February 7th, 2010 at the Tara Retreat Centre on Whidbey Island. The Puja was requested by Tenzin and Katie Denison.</p>
			<p>This traditional Fire Puja is beneficial on many levels; for fulfilling wishes, removing obstacles, improving health and increasing merit and wealth. The blessings of the Puja can also stabilize one’s meditative concentration and improve one’s spiritual practice. The smoke from the fire itself is charged with vibrations that travel for miles creating peace and harmony in the world.</p>
			
<p>For the deceased it is a wonderful way to purify negative Karma and attain higher rebirth. Participants were asked to bring either a photo or the written name of the deceased. Those who were unable to attend were still able to participate by giving offerings and name and photos of deceased loved ones to the Monastery before the event. Amongst all pujas, the fire puja is considered the king of all Pujas. It is the most powerful of all Pujas within Tibetan Buddhism and its purpose is basically to remove all obstacles and its stains-drima to enlightenment. It is for purifying ordinary appearances, ordinary unwholesome actions and especially for repairing broken pledges, promises and vows.</p>  

		</div>
</section>


 



<?php include('footer.php')?>