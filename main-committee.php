<?php include('header.php')?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Namgon Mani Committee</h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <p>Namgon Mani association was from in 2012 with the aims and motive to spread religious in villages of Upper Mustang. Every 5 years the members will change and will hold the responsibility for the next 5 years. Namgon Mani association holds the full responsibility of Mani program organize in Nunnery School, Threngkar every year. Khenpo Tsewang Rigzin leads the Mani program every year.</p>
    <table class="table table-bordered mt-20">
      <thead>
        <tr>
          <th width="5%">S.N.</th>
          <th>Name</th>
          <th>Designation</th>
          <th>Address</th>
          <th>Phone</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1.</td>
          <td>Khenpo Khenrab Sangbo
</td>
          <td>President </td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>2.</td>
          <td>Mr. Sonam Choedup</td>
          <td>Secretary</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>3.</td>
          <td>Ven. Tenpa Gyatso</td>
          <td>Cashier   </td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>4.</td>
          <td>Ms Yangchen Gurung</td>
          <td>Accountant</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>5.</td>
          <td>Mr. Tsering Namgyal Thakuri</td>
          <td>In charge</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>6.</td>
          <td>Ven. Pema Wangdi Gurung</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        <tr>
          <td>7.</td>
          <td>Ven. Gyatso Gurung</td>
          <td>Member</td>
          <td>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</td>
          <td>+977-061-622629</td>
        </tr>
        
        
      </tbody>
    </table>
    
  </div>
</section>
<?php include('footer.php')?>