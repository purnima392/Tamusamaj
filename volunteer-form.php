<?php include('header.php')?>
<style>
ol, li {
  padding: 0;
  margin: 0;
  list-style: none;
}

.registration_form {
}
.inner_wrapper {
 border: 1px solid #ccc;
  padding: 20px;
}
h3 {
  text-align: center;
  background-color: #000;
  max-width: 500px;
  margin: 0 auto 15px auto;
  color: #fff;
  padding: 7px;
}
h4 { font-size: 17px;
    font-weight: 600;}
.datepicker {
  float: right;
}
.quater-width {
  width: 30%;
  float: right;
}
label {
  display: inline-block;
  
  vertical-align: bottom;
  margin-bottom: 0;
  font-size: 15px;
  margin-right: 10px;
}
.form-control {
  border: none;
  border-bottom: 1px dashed #a5a4a4;
  border-radius: 0;
  box-shadow: none;
  display: inline-block;
  width: 70%;
  vertical-align: bottom;
}
.form-control:focus {
  border: none;
  box-shadow: none;
  border-bottom: 1px dashed #a5a4a4;
}
.full-width {
  width: 100%;
}
.title {
  line-height: 26px;
  font-size: 15px;
}
.title span {
  display: block;
}
.upload_img {
 border: 1px solid #ccc;
 display: block;
 overflow: hidden;
 position: relative;
 width: 150px;
  height: 150px;
 padding:10px;
}
.upload_img img {  width:  100%; height:130px;}
.upload_img span { width: 100%;
    display: flex;
    flex-direction: column;
    font-size: 15px;
    font-weight: bold;
    height: 100%;
    justify-content: center;
    text-align: center;
    width: 100%;
 }
.upload_img input[type="file"] {
 height: 100%;
    left: 0;
    opacity: 0;
    position: absolute;
    top: 0;
    width: 100%;
 cursor: pointer;
}
.radio {
  margin: 0;
}
.radio h4 {
  display: inline-block;
  margin-right: 10px;
  vertical-align: middle;
}
.radio label {
  width: 14%;
}
.half-width label {
  
}
.half-width .form-control {
  
}
.half-width.radio label {
  width: 30%;
  padding-left: 0;
}
.registration_form p {
  font-size: 15px;
}
input[type="radio"], input[type="checkbox"] {
  display: none;
}
input[type="radio"] + label span, input[type="checkbox"] + label span {
  display: inline-block;
  width: 19px;
  height: 19px;
  margin: -1px 5px 0 0;
  vertical-align: middle;
  cursor: pointer;
}
input[type="radio"] + label span, input[type="checkbox"] + label span {
  background-color: transparent;
  border: 1px solid #ccc;
}
input[type="radio"]:checked + label span, input[type="checkbox"]:checked + label span {
  background-color: transparent;
}
input[type="radio"] + label span, input[type="radio"]:checked + label span, input[type="checkbox"] + label span ,input[type="checkbox"]:checked + label span {
  -webkit-transition: background-color 0.4s linear;
  -o-transition: background-color 0.4s linear;
  -moz-transition: background-color 0.4s linear;
  transition: background-color 0.4s linear;
}
input[type="radio"]:checked + label span:before, input[type="checkbox"]:checked + label span:before {
  content: "\f00c";
  font-family: FontAwesome;
  width: 17px;
  height: 17px;
  line-height: 17px;
  display: block;
}
table input[type="text"] {
  border: none;
  width: 100%;
      padding: 0;
}
table input[type="text"]:focus {
  border: none;
}
.full-width label {
  width: 100%;
  padding-left: 0;
}
.l_gaurdia label {
  width: 30%;
}
.l_gaurdia label small {
  display: block;
  font-weight: 300;
}
.l_gaurdia .form-control {
  width: 68%;
}
.document {
}
.document label {
  width: 100%;
  margin: 12px 0;
}
.document .checkbox-inline { margin: 5px 0 0 0;}
.document input[type="file"] {
      width:84px;
    overflow: hidden;
    margin-bottom: 12px;
}
.document li {
  margin: 0;
}
.btn {
  border-radius: 0;
    padding: 8px 30px;
    font-size: 16px;
    text-transform: uppercase;
    font-weight: bold;
    text-shadow: 1px 1px 1px #000;
}
textarea.form-control { width: 100%; border: 1px solid #ccc; margin: 10px 0;}
textarea.form-control:focus { border: 1px solid #ccc;}
</style>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Volunteer Application Form</h2>
      </div>
      <div class="col text-right"> <a href="index.php" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <div id="divRegistrationForm" class="registration_form">
  <div class="inner_wrapper">
    <h3>Volunteer Application Form</h3>
    <form method="post" class="form-res clearfix" enctype="multipart/form-data">      
      <div class="row clearfix">
       
        <div class="col-md-12 text-right">
        <input type='hidden' id="hdnPhoto" name="hdnPhoto" value="">
          <div id="divUploadImage" class="upload_img float-right">
            <span>Upload Photo</span>
             <input type="file" id="txtUploadPhoto" name="txtUploadPhoto">
          </div>
        </div>
      </div>
     
     
        <h4>General Information</h4>
        
     
      <div class="form-group">
        <label>Full Name (In block letter):</label>
        <input type="text" id="txtFullName" name="txtFullName" class="form-control">
      </div>
      <div class="form-group">
        <label>Address:</label>
        <input type="text" id="txtAddress" name="txtAddress" class="form-control">
      </div>
      
      <div class="form-group">
        <div class="row">
          <div class="col-md-6 half-width">
            <label>Age:</label>
            <input type="text" id="txtNationality" name="txtNationality" class="form-control">
          </div>
          
          <div class="col-md-6 radio half-width">
            <h4>Sex:</h4>
            <input type="radio" name="rdioSex" id="rdioMale" value="Male">
            <label class="radio-inline" for="rdioMale"><span></span>Male</label>
            <input type="radio" name="rdioSex" id="rdioFemale" value="Female">
            <label class="radio-inline" for="rdioFemale"><span></span>Female </label>
          </div>
        </div>
      </div>
      
      
      <div class="form-group">
        <div class="row">
          <div class="col-md-6">
            <label>Mobile:</label>
            <input type="text" name="txtMobile" class="form-control">
          </div>
          <div class="col-md-6">
            <label class="text-right">Land Line:</label>
            <input type="text" name="txtLandline" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-6">
            <label>Email:</label>
            <input type="text" name="txtEmail" class="form-control">
          </div>
          <div class="col-md-6">
            <label class="text-right">Social Page Link:</label>
            <input type="text" name="txtFB" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-6">
            <label>Sponsor:</label>
            <input type="text" name="txtSponsor" class="form-control">
          </div>
          <div class="col-md-6">
            <label class="text-right">Contact No:</label>
            <input type="text" name="txtSponsorContact" class="form-control">
          </div>
        </div>
      </div>
      <h4>Travel Details</h4>
	 
        <hr>
       
    
          
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th width="30%">Arrival Date</th>
                <th width="30%">Departure Date</th>
                <th colspan="2" width="40%">Volunteering period (days/week(s))</th>
                
              </tr>
              <tr>
                <td><input type="text" name="txtCPTSchool" class="form-control"></td>
                <td><input type="text" name="txtCPTSymbol" class="form-control"></td>
                <td><input type="text" name="txtCPTYear" class="form-control" placeholder="Days"></td>
                <td><input type="text" name="txtCPTMarks" class="form-control" placeholder="Weeks"></td>
                
              </tr>
			  <tr>
                  <td colspan="2"><input type="text" name="txtCPTSchool" class="form-control" placeholder="For additional day (s)"></td>
               
                <td><input type="text" name="txtCPTYear" class="form-control" placeholder="Days"></td>
                <td><input type="text" name="txtCPTMarks" class="form-control" placeholder="Weeks"></td>
                
              </tr>
            </tbody>
          </table>
     <h4>Facilities Utilized</h4>
	 <hr>
        
       
    
          
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th width="25%">Facility</th>
                <th width="25%">Yes</th>
                <th width="25%">No</th>
				<th width="25%">Remarks</th>
                
              </tr>
              <tr>
                <td><input type="text" name="txtCPTSchool" class="form-control" placeholder="Food"></td>
                <td><input type="text" name="txtCPTSymbol" class="form-control"></td>
                <td><input type="text" name="txtCPTYear" class="form-control"></td>
                <td><input type="text" name="txtCPTMarks" class="form-control"></td>
                
              </tr>
			  <tr>
                <td><input type="text" name="txtCPTSchool" class="form-control" placeholder="Room"></td>
                <td><input type="text" name="txtCPTSymbol" class="form-control"></td>
                <td><input type="text" name="txtCPTYear" class="form-control"></td>
                <td><input type="text" name="txtCPTMarks" class="form-control"></td>
                
              </tr>
			  <tr>
                <td><input type="text" name="txtCPTSchool" class="form-control" placeholder="Wifi Internet (Free)"></td>
                <td><input type="text" name="txtCPTSymbol" class="form-control"></td>
                <td><input type="text" name="txtCPTYear" class="form-control"></td>
                <td><input type="text" name="txtCPTMarks" class="form-control" placeholder="If you wish to access"></td>
                
              </tr>
            </tbody>
          </table>
	 <h4>Special skills or experience</h4>
      <hr>
      <p>Write about any special skills and experiences you possess from previous voluntary work, education, employment or through any other activities, hobbies or interests that may be an additional benefit to this specific program.</p>
     <div class="form-group">
    
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" width="100%"></textarea>
  </div>
  <h4>Rules and general guidelines</h4>
  <ul class="list">
		<li>Be on time for breakfast, lunch and dinner.</li>
		<li>Outdoor games are prohibited during class hours.</li>
		<li>The use of indoor games as teaching aids may be permitted in classes.</li>
		<li>Permission from the office is required to take students for outing.</li>
		<li>In no case are volunteers permitted to allow students into their private rooms.</li>
		<li>Playing with mobile phones, cameras, laptops and other electronic devices in class is not allowed.</li>
		<li>The school supplies of notebooks, colors, pens, pencils, etc. shouldn’t be misused.</li>
		<li>The kitchen is restricted to the kitchen staff and hence cooking separate meals or refreshment is not permitted.</li>
		<li>Use of alcohol, tobacco or any recreational drugs within the school premises is strictly prohibited.</li>
		<li>Volunteers are expected to be back at the school by 2100 hrs.</li>
		<li>Volunteer application form should be submitted within two days of receiving it from the office.</li>
		<li>Volunteers are expected to inform a member of the staff if and when they will be away on a day trip or an overnighter.</li>
		<li>Volunteers are expected to dress conservatively.</li>
		<li>All volunteers are required to clear their dues before leaving.</li>
		
</ul>
          
         <h4>Agreement and Signature </h4>  
       <hr>
        
    
      <div class="radio full-width">
        <input type="checkbox" name="rdioDeclaration" id="rdioDeclaration" value="1">
        <label class="radio-inline" for="rdioDeclaration"><span></span>By submitting this application, I, the undersigned, affirm that the facts declared in it are true.</label>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-6">
           
            <input type="text" name="signatureName" class="form-control">
			 <label>Name / Signature of applicant</label>
          </div>
          <div class="col-md-6">
            <label class="text-right">Date:</label>
            <input type="text" name="date" class="form-control">
          </div>
         
        </div>
      </div>
     
     
   
      
      <div class="form-group">
        <button type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-success">Save</button>
      </div>
    </form>
   </div>
  
</div>
  </div>
</section>
<?php include('footer.php')?>