<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			
					
					
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Pal Ewam Namgyal Monastic School </h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						<div class="square-img"><img src="img/56.jpg" alt=""></div>
						<p>Pal Ewam Namgyal Monastic School’s established began with the visions of the Khenpo Tsewang Rigzin. Khenpo la witnessed the harsh living condition and limited educational opportunities to the local children living in Mustang and other remote areas of Nepal. Then Khenpo la made a commitment to establish a monastic school for the greater benefit of these children. On July 10th 2005 Khenpo Tsewang Rigzin initiated the project and opened a monastic school for young novice monks. The school was thus named Pal Ewam Namgyal Monastic School.  The school began with 16 young novice monks from Upper Mustang. This date was chosen as an auspicious day which correlates with the Lord Buddha’s first teaching day for the sentient beings. The school’s main aim is to provide students with government approved education as well as traditional Buddhist Dharma studies. </p>
<p>Pal Ewam Namgyal Monastic School currently range in age from 6 years to 17 years, classes begin with Lower Kindergarten to grade 7. The students are graded according to their educational proficiency.  Currently we have 65 monks with 7 teaching staffs and 5 non- teaching staffs. On completion of their primary education and Dharma studies and students are presented with an opportunity to choose their own career path. Many students choose careers that exemplify their talents and interest. Besides a normal classes and Buddhist class students are free to learn religious painting, mask making, creating sand mandala and other ritual arts. </p>

<p>Western subjects such as English, Nepali, Math, Science, Social Studies, GK and Computer are taught in our school by qualified trained teachers. Every student is given the same opportunity to decide for themselves whether they wish to pursue further monastic studies or other professional skills. Pal Ewam Namgyal Monastic School provides for each student a daily needs including accommodation, fooding, clothing, health assistance, stationary and other requirement free of charge. All the funds needs are fulfilled by the generous contribution made by our dharma friends, sponsors, well-wishers and dear friends around the globe. </p>



								
				
			
		</div>
</section>


 



<?php include('footer.php')?>