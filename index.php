<?php include('header.php')?>
<!--Slider Start-->
<div class="slider-wrapper">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active"> <img class="d-block w-100" src="img/slider.jpg" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <h3>Pal Ewam <span>Namgyal Monastic School</span></h3>
          <p>The School's main aim is to provide students with government approved  education as well as traditional Buddhist Dharma studies.</p>
          <a href="pal-ewam-namgyal-monastic-school.php" class="btn btn-outline">View More</a> </div>
      </div>
      <div class="carousel-item"> <img class="d-block w-100" src="img/137.jpg" alt="Second slide">
        <div class="carousel-caption d-none d-md-block">
          <h3>Pal Ewam <span>Namgyal Monastic School</span></h3>
          <p>The School's main aim is to provide students with government approved  education as well as traditional Buddhist Dharma studies.</p>
          <a href="pal-ewam-namgyal-monastic-school.php" class="btn btn-outline">View More</a> </div>
      </div>
      <div class="carousel-item"> <img class="d-block w-100" src="img/139.jpg" alt="Third slide">
        <div class="carousel-caption d-none d-md-block">
          <h3>Pal Ewam <span>Namgyal Monastic School</span></h3>
          <p>The School's main aim is to provide students with government approved  education as well as traditional Buddhist Dharma studies.</p>
          <a href="pal-ewam-namgyal-monastic-school.php" class="btn btn-outline">View More</a> </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
</div>
<!--Slider End--> 
<!-- Activities Start -->
<section class="activities">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-md-9">
        <div class="row">
          <div class="col-lg-4">
            <div class="act_item"> <i class="act_icon icon icon-Puzzle"></i>
              <div class="act_content">
                <h3><a href="AcademicDetail.php?id=1">Curriculum Activities</a></h3>
                <p>Every Saturday we organize a inter class and inter house Tri-lingual Spelling contest, Debate, Quiz, Elocution and Poem Recitation.</p>
                <a href="AcademicDetail.php?id=1" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="act_item"> <i class="act_icon icon icon-DesktopMonitor"></i>
              <div class="act_content">
                <h3><a href="AcademicDetail.php?id=3">Computer Class</a></h3>
                <p>In this advanced era computer is a major tools to operate every function. In our school we are giving computer class to the senior students.</p>
                <a href="AcademicDetail.php?id=3" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="act_item"> <i class="act_icon icon icon-Cup"></i>
              <div class="act_content">
                <h3><a href="AcademicDetail.php?id=5">Games & Sports</a></h3>
                <p>In our school we are trying to make best shape for our students internally as well as from physically. Every early morning student has to do a morning workout and exercise for half an hour. </p>
                <a href="AcademicDetail.php?id=5" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3">
        <div class="donate-wrapper">
          <h2>Make Donation</h2>
          <p>To Pal Ewam Namgyal School</p>
          <a href="donate.php" class="btn doante-btn">Donate Now</a> </div>
      </div>
    </div>
  </div>
</section>
<!-- Activities End --> 
<!-- Content Start -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="welcome_txt">
          <h2><span>Welcome to</span> Pal Ewam Namgyal Monastic School</h2>
          <p>Pal Ewam Namgyal Monastic School’s establishment began with the visions of the Khenpo Tsewang Rigzin. Khenpo la witnesses the harsh living condition and limited educational opportunities to the local children living in Mustang and other remote areas of Nepal. Then Khenpo la made a commitment to establish a monastic school for the greater benefit of these children. On 10th July 2005, Khenpo Tsewang Rigzin initiated the project and opened a monastic school for young novice monks. The school was thus named Pal Ewam Namgyal Monastic School. </p>
          <a href="pal-ewam-namgyal-monastic-school.php" class="btn">Read More</a> </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-6">
            <div class="message"> <span class="message-tag">Message</span>
              <div class="thumbimg"><img src="img/khenpo.jpg" alt=""></div>
              <div class="message-txt">
                <h3>Khenpo Tsewang Rigzin</h3>
                <p>Through the Blessing of His Holiness Sakya Trinzin, His Eminence Ngor luding Khen Rinpochee and His Eminence Ngor Thartse Khen Rinpoche and tremendous support from the sponsors, I was able to set up this school. </p>
                <a href="founder-message.php" class="btn">Read More</a> </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="message"> <span class="message-tag">School</span>
              <div class="thumbimg"><img src="img/nunnery.jpg" alt=""></div>
              <div class="message-txt">
                <h3>Nunnery School</h3>
                <p>In June 2005, Mrs Francesca Stengel and Maraigrazia, two Italian women met Ven. Khenpo Tsewang Rigzin (the Abbot of Namgyal Monastery and Founder of Monastic School) and the former school Principal Ven. Lekshay Tendhar.</p>
                <a href="pal-ewam-nangon-nunnery-school.php" class="btn">Read More</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content End --> 
<!-- Project Start -->
<section class="content yellow_bg">
  <div class="container">
    <div class="title text-center"> <i class="icon icon-Chart"></i>
      <h3>Our Project</h3>
      <span class="seperator"><i class="shape"></i><i class="shape"></i><i class="shape"></i></span> </div>
    <div class="row">
      <div class="col-lg-4 col-md-4">
        <div class="project">
          <div class="project_img"><img src="img/3.jpg"></div>
          <div class="project-content">
            <h3><a href="ongoing-project-detail.php?id=8">Namgyal Monastery Re-Construction</a></h3>
            <p>After the devastating earth hit in Nepal on 25th April and 2nd May 2015, thousand of houses and hundreds of people were killed in Nepal.  </p>
            <a href="ongoing-project-detail.php?id=8" class="btn">Read More</a> </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div class="project">
          <div class="project_img"><img src="img/6.jpg"></div>
          <div class="project-content">
            <h3><a href="ongoing-project-detail.php?id=9">Shrine Hall in Nunnery School</a></h3>
            <p>Pal Ewam Namgon Nunnery School is situated in Upper Mustang, Threngkar. Currently we have 50 nuns with 6 teaching staffs.</p>
            <a href="ongoing-project-detail.php?id=9" class="btn">Read More</a> </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4">
        <div class="project">
          <div class="project_img"><img src="img/7.jpg"></div>
          <div class="project-content">
            <h3><a href="ongoing-project-detail.php?id=11">Pal Ewam Namgon Meditation Center</a></h3>
            <p>Since our school is a monastic and it is more related with the religious activities. So many of our friends and volunteers around the globe wish to stay for meditation </p>
            <a href="ongoing-project-detail.php?id=11" class="btn">Read More</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content video">
  <div class="container">
    <div class="video-content text-center">
      <h3>Our School is different here</h3>
      <p>Watch Our Video</p>
      <a href="https://www.youtube.com/watch?v=Vk_BqsWRRD4" class="icon icon-Play popup-iframe"></a> </div>
  </div>
</section>
<section class="content blog">
  <div class="container">
    <div class="title text-center"> <i class="icon icon-Blog"></i>
      <h3>Our Blog</h3>
      <span class="seperator"><i class="shape"></i><i class="shape"></i><i class="shape"></i></span> </div>
    <div class="row">
      <div class="col-lg-3 col-md-3">
        <article class="blogpost">
          <div class="blog-img"><img data-src="holder.js/350x200?bg=fbba05"></div>
          <div class="blogpost-body">
            <div class="blog-title">
              <h2 class="title"><a href="#">Fundraiser update</a></h2>
              <div class="post-info"> <span><i class="fa fa-calendar"></i>Dec 28, 2016</span> </div>
              <div class="submitted"><i class="fa fa-user pr-5"></i> by <a href="#">India</a></div>
            </div>
            <div class="blogpost-content">
              <p>Over 70% of target raised – what an amazing response by our friends and supporters,  Thank you all so very much. I’ve been asked by several supporters for an update on progress towards raising the money for buying much needed bunk beds and bedding for our dear girls in their new winter school, so unusually I’m posting another blog this month.</p>
            </div>
            <div class="blog-footer clearfix"> <div class="pull-left"><i class="fa fa-comment-o"></i> <a href="#">22 comments</a> </div><div class="blog-btn pull-right"><a href="#" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div> </div>
          </div>
        </article>
      </div>
      <div class="col-lg-3 col-md-3">
        <article class="blogpost">
          <div class="blog-img"><img data-src="holder.js/350x200?bg=fbba05"></div>
          <div class="blogpost-body">
            <div class="blog-title">
              <h2 class="title"><a href="#">New Winter School Opening</a></h2>
              <div class="post-info"> <span><i class="fa fa-calendar"></i>Dec 11, 2016</span> </div>
              <div class="submitted"><i class="fa fa-user pr-5"></i> by <a href="#">India</a></div>
            </div>
            <div class="blogpost-content">
              <p>Last month we attended an amazing opening ceremony and inauguration of the new winter school building in Pokhara.When we first opened the school in 2012 with 18 girls it was quite easy to afford to rent a property in Pokhara so the girls and staff could relocate from Upper Mustang,  where it becomes too cold to study in the harsh winter conditions. </p>
            </div>
           <div class="blog-footer clearfix"> <div class="pull-left"><i class="fa fa-comment-o"></i> <a href="#">22 comments</a> </div><div class="blog-btn pull-right"><a href="#" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div> </div>
          </div>
        </article>
      </div>
      <div class="col-lg-3 col-md-3">
        <article class="blogpost">
          <div class="blog-img"><img data-src="holder.js/350x200?bg=fbba05"></div>
          <div class="blogpost-body">
            <div class="blog-title">
              <h2 class="title"><a href="#">Mixed News from Nepal</a></h2>
              <div class="post-info"> <span><i class="fa fa-calendar"></i>Oct 15, 2015</span> </div>
              <div class="submitted"><i class="fa fa-user pr-5"></i> by <a href="#">India</a></div>
            </div>
            <div class="blogpost-content">
              <p>As the girls and staff travel down from Mustang to Pokhara for winter school, there is mixed news from Nepal. </p>
            </div>
           <div class="blog-footer clearfix"> <div class="pull-left"><i class="fa fa-comment-o"></i> <a href="#">22 comments</a> </div><div class="blog-btn pull-right"><a href="#" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div> </div>
          </div>
        </article>
      </div>
      <div class="col-lg-3 col-md-3">
        <article class="blogpost">
          <div class="blog-img"><img data-src="holder.js/350x200?bg=fbba05"></div>
          <div class="blogpost-body">
            <div class="blog-title">
              <h2 class="title"><a href="#">Mustang Summer News</a></h2>
              <div class="post-info"> <span><i class="fa fa-calendar"></i>Jul 13, 2015</span> </div>
              <div class="submitted"><i class="fa fa-user pr-5"></i> by <a href="#">India</a></div>
            </div>
            <div class="blogpost-content">
              <p>It has seemed forever waiting for actual news from Mustang after the horrendous earthquake.  Our dear school secretary, Angya, has just returned from a visit and brings welcome news. All the girls are very healthy and happy in school and no injuries were sustained in the earthquake – we are so happy to report confirmation of this. </p>
            </div>
           <div class="blog-footer clearfix"> <div class="pull-left"><i class="fa fa-comment-o"></i> <a href="#">22 comments</a> </div><div class="blog-btn pull-right"><a href="#" class="links">Read More <i class="fa fa-long-arrow-right"></i></a></div> </div>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>
<section class="content sponsor">
  <div class="container">
    <div class="sponsor-title">
      <h3>Our Sponsor</h3>
    </div>
    <div class="sponsor_list">
      <ul>
        <li><a href="http://altevetteschool.org/" target="_blank"><img src="img/altevette.png" alt="Altevette"></a></li>
        <li><a href="http://www.tibetrelieffund.co.uk/" target="_blank"><img src="img/tibet_refund.png" alt="Tibet Refund"></a></li>
        <li><a href="https://lovepalprojectlaos2016.wordpress.com" target="_blank"><img src="img/logo_l.png" alt="Love@Pal"></a></li>
      </ul>
    </div>
  </div>
</section>
<?php include('footer.php')?>