<html>
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <title></title>
  </head>
  <body>
    <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">
    <b>A</b>P</span> 
    <!-- logo for regular state and mobile devices -->
     
    <span class="logo-lg">
      <b>Admin Panel</b>
    </span></a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image" /> 
            <span class="hidden-xs">Admin</span></a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                <p>Admin</p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="logout.php">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav></header>
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li class="active treeview">
            <a href="home">
              <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <span>Projects</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddProjects.php">Add Projects</a>
              </li>
              <li>
                <a href="EditProjects.php">Edit Projects</a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <span>Academics</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddAcademics.php">Add Academics</a>
              </li>
              <li>
                <a href="EditAcademics.php">Edit Academics</a>
              </li>
            </ul>
          </li>
	 <li class="treeview">
            <a href="#">
              <span>Blogs</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddBlog.php">Add Blog</a>
              </li>
              <li>
                <a href="EditBlog.php">Edit Blog</a>
              </li>
            </ul>
          </li>
        <li class="treeview">
            <a href="#">
              <span>Staffs</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddStaff.php">Add Staff</a>
              </li>
              <li>
                <a href="EditStaff.php">Edit Staff</a>
              </li>
            </ul>
          </li>
         <li class="treeview">
            <a href="#">
              <span>Result</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddResult.php">Add Result</a>
              </li>
              <li>
                <a href="EditResult.php">Edit Result</a>
              </li>
            </ul>
          </li>
        <li class="treeview">
            <a href="#">
              <span>Gallery</span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="AddGallery.php">Add Gallery</a>
              </li>
            </ul>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
  </body>
</html>
