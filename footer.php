<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="footer-content">
          <div class="footer-logo">
            <h4>About Us</h4>
            <p>Pal Ewam Namgyal Monastic School’s established began with the visions of the Khenpo Tsewang Rigzin. s</p>
            <a href="#" class="links">Read More</a> </div>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="row">
          <div class="col-md-4 col-sm-4">
            <h4>Connect</h4>
            <ul>
              <li><a href="#">+977-061-622629</a></li>
              <li><a href="#">info@penms.edu.np</a></li>
              <li>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-3">
            <h4>Social Media</h4>
            <ul class="social-links">
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
          </div>
          <div class="col-md-5 col-sm-5">
            <h4>Subscribe to our newsletter</h4>
            <small>We will send you updates on how our team working on new projects and general news.</small>
            <div class="newsletter">
              <form>
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <button type="submit" class="btn btn-primary">Subscribe</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-footer text-center ">
    <div class="container clearfix">
      <p class="copyright float-left">© 2017 Pal Ewam Namgyal Monastic School. All rights reserved</p>
      <p class="float-right">Design by <a href="#">Webpage Nepal</a></p>
    </div>
  </div>
</footer>
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="js/jquery.min.js" type="text/javascript"></script> 
<script src="js/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script> 
<script src="js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> 
<script type='text/javascript' src='js/holder.min.js'></script> 
<script type='text/javascript' src='js/unitegallery.min.js'></script> 
<script type='text/javascript' src='js/ug-theme-tiles.js'></script> 
<script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGe_F0BQzzAueb0B4ZAUTWPv3Nejp0a7o&callback=initMap"
    async defer></script>
    <script src="js/googlemap.js"></script> 
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>