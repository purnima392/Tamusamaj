<?php include('header.php');
	require('AdminLTE/inc/config.php');
	$CategoryId=$_GET['id'];
	if($CategoryId==1)
	 $ProjectTitle="Ongoing Project";
	else if($CategoryId==2)
	 $ProjectTitle="Upcoming Project";
	else if($CategoryId==3)
	 $ProjectTitle="Future Project";
	
?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle"><?=$ProjectTitle?></h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div class="row">
			<?php
				$latProjects=$mysqli->query("select * from projects where CategoryId=$CategoryId");
				while($SiPackage=$latProjects->fetch_array()){
					$ProjectId=$SiPackage["ProjectId"];
					$ProjectName=$SiPackage["ProjectName"];
					$Description=$SiPackage["Description"];
					$Photo=$SiPackage["Photo"];
				?>
				<div class="col-lg-4 col-md-4">
					<div class="project">
						<div class="project_img"><img src="img/<?=$Photo?>"></div>
						<div class="project-content">
							<h3><a href="ongoing-project-detail.php?id=<?=$ProjectId?>"><?=$ProjectName?></a></h3>
							<div class="Description"><?=$Description?></div>
						<a href="ongoing-project-detail.php?id=<?=$ProjectId?>" class="btn">Read More</a> </div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>






<?php include('footer.php')?>