<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			
					
				
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Message from Khenpo Tsewang Rigzin <span>(Founder)</span>  </h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						<div class="circle-img"><img src="img/khenpo.jpg" alt=""></div>
						<p>Tashi Delek and Namaste to all my dear friends and well-wishers. I ‘’Khenpo Tsewang Rigzin’’ founder of Pal Ewam Namgyal Monastic School, Pal Ewam Namgon Nunnery school and abbot of Pal Ewam Namgyal Choedhe Thupten Dhargyeling Monastery, would like to convey my mission and fourteen years of experiences  in this school in brief. </p>
<p>Through the Blessing of His Holiness Sakya Trinzin, His Eminence Ngor luding Khen Rinpochee and His Eminence Ngor Thartse Khen Rinpoche and tremendous support from the sponsors, I was able to set up this school. I have been honoured and privileged to serve in this school for fourteen years and will be so on.</p>
<p>As we know that in this twenty-first century, education plays a key role in the life of every sentient being. In Nepal according to the national survey 40% of the people are literate. Likewise in Upper Mustang is one the most remote areas of Nepal where there are limited schools and lack of facilities which is the basic needs for every child. So, that is why I set up this school in Upper Mustang through the financial help from sponsors, I was able to carry on till now. Even yours single penny means a lot us and we are using it very wisely in educational purpose, fooding, medical stuff, clothing etc.</p>

<p>I would like to deeply thank from my depth of heart  to Altevette Onlus (Italy), Tibet Relief fund (UK), Mr. Ong Wee Yeap (Love@Pal), Sister Blenda Chan and all the individual sponsors for your consistent support and love for us. I am sure that in the coming years also you will keep continuing to support for us. Thus I would like call you a light of Mustang. </p>
<p>I always pray to our Triple Gem for your good health, prosperous in every field and brings success throughout your life.</p>
<b>Thank you all !</b>


								
					
		
		</div>
</section>


 



<?php include('footer.php')?>