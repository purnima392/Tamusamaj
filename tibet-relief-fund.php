<?php include('header.php')?>
<section class="content inner-content">
	<div class="container">
		
		
		
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">Tibet Relief Fund (UK)</h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>S.No.</th>
					<th>Sponsor Name</th>
					<th>Supporter ref</th>
					<th>Student Name</th>
					<th>Student ref</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td>Miss Ruth Tarling</td>
					<td>1042</td>
					<td>Ngawang Sangpo</td>
					<td>TRF/NM29</td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Dr. Roger Taylor</td>
					<td>1047</td>
					<td>Nyima Gyaltsen</td>
					<td>TRF/NM4</td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Mr. Martin Kemp & Mrs sue Dawakins</td>
					<td>3390</td>
					<td>Ngawang Lodoe</td>
					<td>TRF/NM17</td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Mr. Martin Kemp & Mrs Sue Dawkins</td>
					<td>3390</td>
					<td>Yeshi Dorje</td>
					<td>TRF/NM16</td>
				</tr>
				<tr>
					<td>5.</td>
					<td>Ms Angela Webb</td>
					<td>5046</td>
					<td>Ngawang Choedak</td>
					<td>TRF/NM6</td>
				</tr>
				<tr>
					<td>6.</td>
					<td>Ms Carolle Payne</td>
					<td>5484</td>
					<td>Lungri</td>
					<td>TRF/NM8</td>
				</tr>
				<tr>
					<td>7.</td>
					<td>Mr Matthew Vinall</td>
					<td>7131</td>
					<td>Palden Gyaltsen</td>
					<td>TRF/NM23</td>
				</tr>
				<tr>
					<td>8.</td>
					<td>Mrs Feroza Bannister</td>
					<td>7162</td>
					<td>Sammer Gurung</td>
					<td>TRF/NM32</td>
				</tr>
				<tr>
					<td>9.</td>
					<td>Mr Paul Green</td>
					<td>7578</td>
					<td>Jamyang Tendhar</td>
					<td>TRF/NM14</td>
				</tr>
				<tr>
					<td>10.</td>
					<td>Mrs Devam Handry</td>
					<td>9210</td>
					<td>Pasang</td>
					<td>TRF/NM25</td>
				</tr>
				<tr>
					<td>11.</td>
					<td>Mr Stephen Silver</td>
					<td>9737</td>
					<td>Ngawang Pasang</td>
					<td>TRF/NM22</td>
				</tr>
				<tr>
					<td>12.</td>
					<td>Mr David Flinstone</td>
					<td>11289</td>
					<td>Karma Tsundue</td>
					<td>TRF/NM33</td>
				</tr>
				<tr>
					<td>13.</td>
					<td>Mr Amer Yousaf</td>
					<td>11691</td>
					<td>Choekyi Wangpo</td>
					<td>TRF/NM9</td>
				</tr>
				<tr>
					<td>14.</td>
					<td>Dr Robert Oulton</td>
					<td>12190</td>
					<td>Ngawang Choephel</td>
					<td>TRF/NM10</td>
				</tr>
				<tr>
					<td>15.</td>
					<td>Ms Sally Greenaway</td>
					<td>13536</td>
					<td>Choekyong Sangpo</td>
					<td>TRF/NM28</td>
				</tr>
				<tr>
					<td>16.</td>
					<td>Mrs Gail Stuart</td>
					<td>13601</td>
					<td>Tenpa Sangpo</td>
					<td>TRF/NM12</td>
				</tr>
				<tr>
					<td>17.</td>
					<td>Ms Roseanna Lawrence</td>
					<td>14933</td>
					<td>Konchok Rinchen</td>
					<td>TRF/NM31</td>
				</tr>
				<tr>
					<td>18.</td>
					<td>Mrs Tatiana Joseph</td>
					<td>16929</td>
					<td>Choekyi Gyatso</td>
					<td>TRF/NM5</td>
				</tr>
				<tr>
					<td>19.</td>
					<td>Mrs Carolyn Edrich</td>
					<td>17787</td>
					<td>Jamyang Kunga</td>
					<td>TRF/NM3</td>
				</tr>
				<tr>
					<td>20.</td>
					<td>Mr Neil Bowman &Ms Carole Mahoney</td>
					<td>18159</td>
					<td>Choekyi Lodoe</td>
					<td>TRF/NM1</td>
				</tr>
				<tr>
					<td>21.</td>
					<td>Mr Mike Crompton</td>
					<td>19420</td>
					<td>Tenzin Phuntsok Sherpa</td>
					<td>TRF/NM27</td>
				</tr>
				
				<tr>
					<td>22.</td>
					<td>Mr Mike Crompton</td>
					<td>19420</td>
					<td>Pasang Tenpa Sherpa</td>
					<td>TRF/NM26</td>
				</tr>
				<tr>
					<td>23.</td>
					<td>Mr Josh Barber</td>
					<td>19605</td>
					<td>Choekyi Palden</td>
					<td>TRF/NM</td>
				</tr>
				<tr>
					<td>24.</td>
					<td>Ms Wendy Bates</td>
					<td>19779</td>
					<td>Tashi Wangdue</td>
					<td>TRF/NM30</td>
				</tr>
				<tr>
					<td>25.</td>
					<td>Mr Cameron Torbet</td>
					<td>11131</td>
					<td>Tsewang Dorje</td>
					<td>TRF/NM24</td>
				</tr>
				<tr>
					<td>26.</td>
					<td>Mr Rick Van der Hammen</td>
					<td>25162</td>
					<td>Dawa Tsering</td>
					<td>TRF/NM21</td>
				</tr>
				<tr>
					<td>27.</td>
					<td>Mr Simon Maxwell</td>
					<td>25723</td>
					<td>Pema Tsering</td>
					<td>TRF/NM21</td>
				</tr>
				
			</tbody>
		</table>
		
		
		
		
	</div>
</section>






<?php include('footer.php')?>