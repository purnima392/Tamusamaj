<?php include('header.php');
      require('AdminLTE/inc/config.php');
?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">Gallery</h2>
			</div>
			<div class="col text-right">
				<a href="index.php" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div id="gallery" style="display:none;">
		<?php
            $latProjects=$mysqli->query("SELECT * FROM gallery");
            while($SiPackage=$latProjects->fetch_array()){
			$GalleryId=$SiPackage["GalleryId"];
			$Photo=$SiPackage["Photo"];
	    ?>
			<a href="#">
				<img alt="Image"
				src="img/<?=$Photo?>" 
				data-image="img/<?=$Photo?>" 
				data-image-mobile="img/<?=$Photo?>" 
				data-thumb-mobile="img/<?=$Photo?>" 
				data-description=""
				style="display:none"> 
			</a>
		<?php } ?>
		</div>
	</div>
</section>

<?php include('footer.php')?>