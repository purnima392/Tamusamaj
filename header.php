<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Pal Ewam Namgyal Monastic School</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css" >
<link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<!--Icon Fonts-->
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">
<link href="css/stroke-gap-icon.css" rel="stylesheet" type="text/css">
<link rel='stylesheet' href='css/unite-gallery.css' type='text/css' />
<link rel='stylesheet' href='css/magnific-popup.css' type='text/css' />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!--Header Start-->
<header>
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="left-top">
            <ul>
              <li><span class="icon icon-Pointer"></span>Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal</li>
              <li><span class="icon icon-Phone2"></span>+977-061-622629</li>
            </ul>
          </div>
        </div>
        <div class="col">
          <div class="right-top text-right">
            <ul class="social-links">
              <li><a href="https://www.facebook.com/penms.edu.np" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mid-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <h1 class="logo"><a href="index.php"><img src="img/logo.png" alt=""></a></h1>
		  <h2>༄༄། དཔལ་ཨེ་ཝཾ་རྣམ་དགོན་སློབ་གྲྭ།
</h2>
        </div>
		
        <div class="col-lg-5 text-right pt-20 btn-wrapper"> <a class="btn donate-btn" href="donate.php"><i class="icon donate-icon"><img src="img/66185.png" alt="donate"></i>Donate Here</a> <a class="btn volunteer-btn" href="volunteer-form.php"><i class="icon volunteer-icon"><img src="img/4060-200.png" alt="volunteer"></i>Become a Volunteer</a> </div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container"> <a class="navbar-brand" href="#">Menu</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active"> <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
          <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> About Us</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="objectives.php">Our Aims & Objectives</a> <a class="dropdown-item" href="founder-message.php">Message from Founder Director </a> <a class="dropdown-item" href="pal-ewam-namgyal-choede-thupten-dhargyeling-monastery.php">Pal Ewam Namgyal Choede Thupten Dhargyeling Monastery</a> <a class="dropdown-item" href="pal-ewam-namgyal-monastic-school.php">Pal Ewam Namgyal Monastic School </a> <a class="dropdown-item" href="pal-ewam-nangon-nunnery-school.php">Pal Ewam Nangon Nunnery School </a> <a class="dropdown-item" href="welfare-committee.php">Namgon Welfare Committee</a> <a class="dropdown-item" href="main-committee.php">Namgon Mani Committee</a> <a class="dropdown-item" href="religious-activities.php">Religious Activities</a> </div>
          </li>
          <li class="nav-item dropdown"> 
           <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Staff</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
            <a class="dropdown-item" href="admin-staff.php?id=1">Admin Staffs</a> 
            <a class="dropdown-item" href="admin-staff.php?id=2">Teaching Staffs</a> 
            <a class="dropdown-item" href="admin-staff.php?id=3">Non Teaching Staffs </a> </div>
          </li>
          <li class="nav-item dropdown"> 
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Project</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
            <a class="dropdown-item" href="ongoing-project.php?id=1">Latest Project</a> 
            <a class="dropdown-item" href="ongoing-project.php?id=2">Upcoming Project</a>
            <a class="dropdown-item" href="ongoing-project.php?id=3">Future Project </a> 
            </div>
          </li>
          <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Academic</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="AcademicDetail.php?id=1">Co-curriculum Activities</a> <a class="dropdown-item" href="AcademicDetail.php?id=2">Exam</a> <a class="dropdown-item" href="AcademicDetail.php?id=3">Computer Class</a> <a class="dropdown-item" href="AcademicDetail.php?id=4">Result</a> <a class="dropdown-item" href="AcademicDetail.php?id=5">Games & Sports</a> </div>
          </li>
          <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Volunteer Programme </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="how-to-join.php">How to Join</a> <a class="dropdown-item" href="volunteer-form.php">Form</a> </div>
          </li>
          <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Sponsorship </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="altevette.php">Altevette Onlus (Italy)</a> <a class="dropdown-item" href="tibet-relief-fund.php">Tibet Relief Fund (UK)</a> <a class="dropdown-item" href="https://lovepalprojectlaos2016.wordpress.com" target="_blank">Love @ Pal (Singapore)</a> </div>
          </li>
          <li class="nav-item"><a class="nav-link" href="blog.php">Articles/Blogs </a></li>
          <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
          <li class="nav-item"><a class="nav-link" href="contact.php">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<!--Header End--> 