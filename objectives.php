<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			<div class="row">
					<div class="col-lg-6 col-md-6">
							<div class="fixed-img">
								<img src="img/ui.jpg" alt="">
							</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Aims & Objestives</h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						<p class="lead">To provide free education and complete care for the children of the most destitute families in Mustang, Solukhumbu, Dolpa, Manang and Gorkha etc, through traditional monastic education combined with a modern secular education.</p>
								<ul class="list">
									<li>To provide free education and complete care for the children of the most destitute families in Mustang, Solukhumbu, Dolpa, Manang and Gorkha etc, through traditional monastic education combined with a modern secular education.</li>
									<li>To educate and nurture the Buddhism for well beings of all the monks and nuns to restore the ancient monastery and its infrastructure.</li>
									<li>To preserve and promote Buddha Dharma and culture by keeping the religious harmony intact.</li>
									<li>To preserve and maintain the Buddhist traditional ritual practices and Buddhist religious activities in the monastery.</li>
									<li>To provide Buddhist education to the monks and nuns in order to work  for the welfare of monastery and community as well as to spread the Buddha’s teaching and peace path in the this world.</li>
									<li>To create a model for the community, where education is highly valued by providing a better opportunity for learning through discipline, dedication and hard work.</li>
									<li>To inspire an awakening among the community to preserve its culture identity and ethical values in this changing world.</li>
									<li>To awaken a consciousness in the heart of the monks so they will be a responsible part of larger community and uplift all humanity through prayers and blessings.</li>
									<li>To provide the privilege of full range of monastic traditional linguistic, philosopher and meditation training to monks. With this training they will be able to teach.</li>
									<li>To provide an environment of comprehensive western curriculum to students along with religious education.</li>
									
								</ul>
								
					</div>
			</div>	
		</div>
</section>


 



<?php include('footer.php')?>