<?php include('header.php')?>
 <section class="content inner-content">
		<div class="container">
			
					
					
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle">Pal Ewam Namgyal Choede Thupten Dhargyeling Monastery</h2>
						</div>
							<div class="col text-right">
								<a href="index.php" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
								
							</div>
						</div>
						<div class="square-img"><img src="img/56.jpg" alt=""></div>
						<p>Pal Ewam Namgyal Thupten Dhargyeling Monastery is one of the oldest historical monasteries is the Upper Mustang, where huge localities come to worshipped and performed religious ritual and tradional performances. The monastery is located in the northern range of Nepal close to the Tibet border, at the altitude of 3850 meters from sea level. It takes 1 day drive from the Headquarter of Mustang, Jomsom.</p>
<p>The monastery was established in the 13th century during the Ngor tradition of Sakyapa sub-sect by the great Vajra Ngorchen Kunga Sangpo. This monastery was established by accumulating four great monasteries of Ngor tradition like Phuphak Samten Ling, Rishing Domsum Ling, Jampa Shedrup Ling and Drakar Theckchen Ling. These monasteries were founded in 1310 (5th cycle of Tibetan calendar) and were established before the birth of Ngorchen Kunga Sangpo. It is also said and believes that the teachings of Sutrayana and Tantrayana flourished to a great extent in those monasteries.</p>

<p>Namgyal monastery acts as the key holder of the four great ancient monasteries. In additional, Ngorchen Kunga Sangpo brought into existence of spiritual community. He sustained this community for imparting instruction, recitation and empowerment. He ripened many practitioners according to the four great secret tantras. In the year 1428, the great scholars Shakya Chokden were invited to Mustang where he imparted teaching and recitation of the Manjushree Sutra and the four great secret tantras to more than thousands of people in the presence of the Royal King Tashi Gon and the Prince of Mustang. At the same time the king was afraid of sudden attack by southern and Northern Province then he pleaded with the great scholar Sakya Chokden to reverse the situation. In accordance with the king’s request Rinpoche performed regular Torma offering for certain period and focused his mind on the four headed Mahakala, after a while king’s unfavorable situation was improved. </p>

<p>After years gradually all the regular orders and tradition of the monastery was declined and number of monks were decreased due to lack of proper care and rare visit of high lamas. So in order to regain the monastic tradition and practices fortunately Late H.E. Chogye Trichen Rinpoche visited Upper Mustang and his religious visit and spiritual activities in the monastery has been able to maintain and preserved once again. After years back in 2004, spiritual guidance from His Holiness Sakya Trizin and kind request from monastic Sangha community Venerable Khenpo Tsewang Ringin has come dot hold the responsibility of Namgyal monastery to maintain and preserve the monastic tradition and ritual practices. And according to his wish and strong determination he has achieved a lot than he has expected. His strong dedication, enthusiasm, contribution and hard work come out with outstanding results today. </p>

<p>Unfortunately, massive earthquake hit on April 25th 2015 in Nepal Namgyal monastery was badly destroyed and collapsed. Everything was messed up and there was nothing left but fortunately no humans were harmed. </p> 

								
				
			
		</div>
</section>


 



<?php include('footer.php')?>