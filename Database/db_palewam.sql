-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2017 at 09:42 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_palewam`
--

-- --------------------------------------------------------

--
-- Table structure for table `academics`
--

CREATE TABLE IF NOT EXISTS `academics` (
  `AcademicId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` text NOT NULL,
  `Description` text NOT NULL,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`AcademicId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `academics`
--

INSERT INTO `academics` (`AcademicId`, `Title`, `Description`, `Photo`) VALUES
(1, 'Co-curriculum Activites', '<p>Every Saturday we organize a\r\ninter class and inter house Tri-lingual Spelling contest, Debate, Quiz,\r\nElocution and Poem Recitation. The motive of organizing such activities is to\r\nmake confident in the public, reading, speaking etc. Students are making a huge\r\nprogress and develop after every Saturday. Sometime we call a special guest\r\njudge from outside to observe our student knowledge and their creativity.\r\nLikewise, students from higher class are getting opportunity to watch different\r\ndocumentary movies in our conference through projector. </p>\r\n\r\n\r\n\r\n\r\n\r\n<br>', '1504607153g.jpg'),
(2, 'Exam', '<p>In a year we have three terminal\r\nexams after every four months. Each term exam is very important for students to\r\nmove in their next grade. Besides the term exam, our teacher will also take a\r\nmonthly and weekly test in their class to check the student capability. It is\r\nnot only to check the students knowledge but also gives a idea to teacher that\r\nhow teaching method should implement to each students. </p><br>', '1504608648sc.jpg'),
(3, 'Computer Class', '<p>In this advanced era computer is\r\na major tools to operate every function. In our school we are giving computer\r\nclass to the senior students. Although we have limited computers now but in the\r\nfuture we are planning to add more if we find any donors. Basics computer like\r\nMS Word, Typing and Excel are taught by our teachers. </p><br>', '1504608743sc.jpg'),
(4, 'Games and Sports', '<p>In our school we are trying to\r\nmake best shape for our students internally as well as from physically. Every\r\nearly morning student has to do a morning workout and exercise for half an\r\nhour. At the same time different games are organize during the weekend.\r\nBasketball match, volley match and football match are organize between inter\r\nhouse to make our physical fit and strong. Sometime our senior students are\r\nsent you other schools for games competition. </p>\r\n\r\n\r\n\r\n\r\n\r\n <br>', '1504608853beds-prep-1-resize-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `BlogId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(5000) NOT NULL,
  `Description` varchar(40000) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BlogId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`BlogId`, `Title`, `Description`, `Photo`, `CreatedOn`) VALUES
(2, 'Fundraiser update', '<div>\r\n              <p>Over 70% of target raised â€“ what an amazing response by our friends and supporters,  Thank you all so very much.</p>\r\n			  <p>Iâ€™ve been asked by several supporters for an update on progress \r\ntowards raising the money for buying much needed bunk beds and bedding \r\nfor our dear girls in their new winter school, so unusually Iâ€™m posting \r\nanother blog this month.</p>\r\n			  <p>Weâ€™re so grateful to all the contributions generously made so \r\nfar.  Between us weâ€™ve raised over 3,300 Euros towards our 5,000 target.\r\n  The great news is that weâ€™ve almost raised enough to pay for the bunk \r\nbeds â€“ please help us reach the target soon so we can buy bedding too.  \r\n We heard from school staff that winter nights are very cold now in \r\nNepal so we felt a great urgency to help the girls off the floor and \r\ninto beds as soon as we could.  So the order for the beds has already \r\nbeen placed with the manufacturer and weâ€™re hoping for delivery soon.</p>\r\n			  <p>Manufacturing progress  is going well â€“ many of the beds have been built and are being painted now.</p>\r\n			  \r\n			  <p>We also need funds to pay for 58 sets of bedding too as the \r\nfloor mattresses wont fit the beds.  So if any of you, dear supporters, \r\n have friends or family members that may also be interested in \r\ncontributing please can I ask you to share this information with them?</p>\r\n			  <p>Some supporters have asked how far their $$$, â‚¬â‚¬â‚¬ or Â£Â£Â£ will go\r\n â€“ and the answer is quite a long way in Nepal!  All donations will be \r\nvery gratefully received â€“ a gift of 5 Euro will buy a pillow and cover,\r\n 15 Euro will help us buy a warm blanket and 30 Euro a mattress.  And \r\naround 80 Euro will fund a bunk bed for 2 girls and 60 Euro will buy a \r\nsingle bed for teachers or support staff.</p>\r\n			  \r\n            </div>\r\n\r\n<br>', '1504603543project.jpg', '2017-09-06 10:07:55'),
(4, 'New Winter School Opening', 'Last month we attended an amazing opening ceremony and inauguration of the new winter school building in Pokhara.When we first opened the school in 2012 with 18 girls it was quite easy to afford to rent a property in Pokhara so the girls and staff could relocate from Upper Mustang,  where it becomes too cold to study in the harsh winter conditions.', '1504603543project.jpg', '2017-09-06 10:17:15'),
(5, 'Mixed News from Nepal', 'As the girls and staff travel down from Mustang to Pokhara for winter school, there is mixed news from Nepal. <br>', '1504603543project.jpg', '2017-09-06 10:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `CommentId` int(11) NOT NULL AUTO_INCREMENT,
  `Comment` varchar(10000) NOT NULL,
  `BlogId` int(11) NOT NULL,
  `CommentedBy` varchar(10000) NOT NULL,
  `Created_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Subject` varchar(1000) NOT NULL,
  PRIMARY KEY (`CommentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`CommentId`, `Comment`, `BlogId`, `CommentedBy`, `Created_Date`, `Subject`) VALUES
(1, 'test message', 2, 'Dipika', '2017-09-07 10:07:32', 'test'),
(2, 'test message', 2, 'Dipika', '2017-09-07 10:07:44', 'test'),
(3, 'test message', 2, 'Dipika', '2017-09-07 10:07:59', 'test'),
(4, 'test message', 2, 'Dipika', '2017-09-07 10:10:23', 'test'),
(5, 'sdf', 2, 'Ceetu', '2017-09-07 10:11:29', 'hel');

-- --------------------------------------------------------

--
-- Table structure for table `cp_admin`
--

CREATE TABLE IF NOT EXISTS `cp_admin` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(256) NOT NULL,
  `Password` varchar(256) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cp_admin`
--

INSERT INTO `cp_admin` (`Id`, `Username`, `Password`) VALUES
(1, 'dipika', 'agrawal');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `GalleryId` int(11) NOT NULL AUTO_INCREMENT,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`GalleryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`GalleryId`, `Photo`) VALUES
(4, '136.jpg'),
(5, '139.jpg'),
(6, '1504608648sc.jpg'),
(7, '1504608743sc.jpg'),
(8, '1504608853beds-prep-1-resize-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `ProjectId` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectName` varchar(5000) NOT NULL,
  `Description` text NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  PRIMARY KEY (`ProjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`ProjectId`, `ProjectName`, `Description`, `CategoryId`, `Photo`) VALUES
(1, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 1, '1504603350project.jpg'),
(2, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 1, '1504603437project.jpg'),
(3, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 2, '1504603505project.jpg'),
(4, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\n\n<br>', 3, '1504603543project.jpg'),
(5, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 3, '1504606803project.jpg'),
(6, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 3, '1504606816project.jpg'),
(7, 'NANJIA TEMPLE RECONSTRUCTION', '<p>Nepal suffered heavy earthquakes on 25 April and 2 May 2015, and thousands of houses collapsed and hundreds of people were buried and suffered heavy casualties. The earthquake also seriously affected the Nanjia Temple. Fortunately, the monks of the temple are safe and sound. Our Kemp Proposal to rebuild the temple as soon as possible. We started the reconstruction work from April 15, 2016. Enthusiastic fund-raising so that we successfully completed about 7% of the reconstruction project. Now we have the following major projects need to raise funds to donate:</p><p><b>Painting works</b></p><p>After the main structure of the temple and the completion of the building, the temple inside the painting and Thangka drawing project also need a lot of money. Approximately 40,00,000 rupees (US $ 40,000). This is because the temple is located in Nepal on the Mu Si Tang, the road is difficult, delivery is also very difficult.</p><p><b>The interior works</b></p><p>It is now necessary to build the scriptures and other instruments of the Buddha and the scriptures. Great Church is the center of the temple, is the most important part.</p><p><b>6.5 feet of Maitreya Buddha</b></p><p>After the restoration of the monastery, the temple will be in the Great Church by a 6.5-foot Maitreya Buddha, because we know Maitreya Buddha, that is, the Buddha, that is, the future of the Buddha.</p>\r\n\r\n<br>', 3, '1504606833project.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `ResultId` int(11) NOT NULL AUTO_INCREMENT,
  `SymbolNo` varchar(256) NOT NULL,
  `Filename` varchar(256) NOT NULL,
  PRIMARY KEY (`ResultId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `StaffId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) NOT NULL,
  `Designation` varchar(256) NOT NULL,
  `Photo` varchar(256) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `SubCategory` varchar(256) NOT NULL,
  PRIMARY KEY (`StaffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`StaffId`, `Name`, `Designation`, `Photo`, `CategoryId`, `SubCategory`) VALUES
(1, 'Khenpo Tsewang Ringin Gurung', 'Founder Director', '', 1, ''),
(2, 'Khenpo Khenrab Sangpo', 'Chairman', '', 1, ''),
(3, 'Ven. Pema Wangdi Gurung', 'Principal', '', 1, ''),
(4, 'Ven. Gyatso Gurung ', 'Vice Principal', '', 1, ''),
(5, 'Mr. Sonam Lama ', 'Secretary', '', 1, ''),
(6, 'Mr. Tamding Tsering ', 'Headmaster', '', 2, 'Monastic'),
(7, 'Mr. Tenzing Darpo ', 'English Teacher', '', 2, 'Monastic'),
(8, 'Mr. J.B. Kunwar ', 'Nepali Teacher', '', 2, 'Monastic'),
(9, 'Miss Pema Choedon ', 'Tibetan Teacher', '', 2, 'Monastic'),
(10, 'Miss Dawa Choezom ', 'Science Teacher', '', 2, 'Monastic'),
(11, 'Miss Deepa Gurung ', 'Math Teacher', '', 2, 'Monastic'),
(12, 'Ven. Lobsang Gurung ', 'Dharma Teacher', '', 2, 'Monastic'),
(13, 'Ven. Tenpa Gyatso ', 'Headmaster', '', 2, 'Nunnery'),
(14, 'Miss Yangchen Gurung ', 'Science Teacher', '', 2, 'Nunnery'),
(15, 'Miss Yangchen Dolker ', 'Teacher', '', 2, 'Nunnery'),
(16, 'Miss Karchung Mentok ', 'English Teacher', '', 2, 'Nunnery'),
(17, 'Miss Tsering Youdon', 'Teacher', '', 2, 'Nunnery'),
(18, 'Miss Maina Kumari ', 'Nepali Teacher', '', 2, 'Nunnery'),
(19, 'Pema Dorjee ', 'Store Incharge', '', 3, 'Nunnery'),
(20, 'Dawa Tsring Gurung ', 'Cook', '', 3, 'Nunnery'),
(21, 'Tsering Namgyal Thakuri ', 'Store Incharge', '', 3, 'Monastic'),
(22, 'Lobsang Tsedar ', 'Cook', '', 3, 'Monastic'),
(23, 'Sonam Choemphel', ' Cook', '', 3, 'Monastic'),
(24, 'Ram Bahadur Tamang', ' Carpenter', '', 3, 'Monastic'),
(25, 'Maya BK', ' Dishwasher', '', 3, 'Monastic');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
